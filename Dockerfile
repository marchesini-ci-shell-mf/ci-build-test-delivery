FROM ubuntu

WORKDIR /applicazione

COPY /src/app .

CMD ["./app"]